package ru.t1.godyna.tm.service;

import ru.t1.godyna.tm.api.repository.ICommandRepository;
import ru.t1.godyna.tm.api.service.ICommandService;
import ru.t1.godyna.tm.model.Command;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
