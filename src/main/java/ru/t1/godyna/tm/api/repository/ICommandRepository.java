package ru.t1.godyna.tm.api.repository;

import ru.t1.godyna.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
